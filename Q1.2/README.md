Please port the following C++ code to JavaScript

```
// language: C++
// environment: production

// generate temporary user id
// the temporary user id must be 18 decimal digits long, and starts with "10"
// so we are left with 16 digits, or 53 bits
int64_t cur_time = get_unix_time_in_milliseconds() & 0x3ffffffffff; // this is 42 bits, we're left with 11 bits
int64_t random   = arc4random_uniform(0x7ff) & 0x7ff;

user_id = 100000000000000000 | (cur_time << 11) | random;
```

