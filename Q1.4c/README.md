Please check the following 2 web pages:

1. https://beacons.ai/animotakuhope
2. https://jookstogo.com/

How would you design a frontend system to support both pages - each page can be further customized with different themes and content?

Please write down your system design and implement one of pages.

You can use the example project below `themed-sites` (`npm init nuxt-app themed-sites`) as a starting point.